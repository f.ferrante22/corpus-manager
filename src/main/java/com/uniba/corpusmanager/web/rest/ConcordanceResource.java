package com.uniba.corpusmanager.web.rest;

import com.uniba.corpusmanager.service.ActionLogService;
import com.uniba.corpusmanager.service.CorpusService;
import com.uniba.corpusmanager.service.dto.ActionLogDTO;
import com.uniba.corpusmanager.service.dto.ConcordanceResultDTO;
import com.uniba.corpusmanager.web.rest.errors.InvalidInputDateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.Nullable;
import java.util.Date;

import static com.uniba.corpusmanager.domain.enumeration.Action.CONCORDANCE;

/**
 * REST controller for managing {@link com.uniba.corpusmanager.domain.Corpus}.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class ConcordanceResource {

    private final ActionLogService actionLogService;
    private final CorpusService corpusService;

    public ConcordanceResource(ActionLogService actionLogService, CorpusService corpusService) {
        this.actionLogService = actionLogService;
        this.corpusService = corpusService;
    }

    @GetMapping("/concordance/result")
    public ResponseEntity<ConcordanceResultDTO> getCollocationResult(
        @RequestParam Long corpusId,
        @RequestParam String search,
        @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable Date startDate,
        @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable Date endDate
    ) {
        log.debug("REST request to get concordance result for corpus {}, search {}, start date {}, end date {}",
            corpusId, search, startDate, endDate
        );
        if (startDate != null && endDate != null && startDate.after(endDate)) {
            throw new InvalidInputDateException();
        }
        String requestPath = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
        actionLogService.save(
            ActionLogDTO.builder()
                .corpusId(corpusId)
                .corpusName(corpusService.findOne(corpusId).get().getName())
                .action(CONCORDANCE)
                .request(requestPath.substring(requestPath.indexOf("/api") + 4))
                .build()
        );
        return ResponseEntity.ok().body(FakeData.fakeConcordance());
    }
}
