import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Moment } from 'moment';
import { TimeSeriesResultService } from 'app/operations/time-series/time-series-result/time-series-result.service';

@Component({
  selector: 'jhi-time-series-result',
  templateUrl: './time-series-result.component.html',
  styleUrls: ['./time-series-result.component.scss']
})
export class TimeSeriesResultComponent implements OnInit {
  options: any;
  corpusId?: number;
  searchTerms?: string;
  firstTerms?: string[];
  secondTerms?: string[];
  terms = '';
  startDate?: Moment;
  endDate?: Moment;
  shiftingPointsEnabled = false;
  noShiftingPointOptions = {
    legend: {
      data: ['trovato', 'online'],
      align: 'left'
    },
    tooltip: {},
    xAxis: {
      type: 'category',
      data: ['1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        name: 'trovato',
        type: 'line',
        data: [75898, 77415, 77444, 77544, 79015, 81578, 86321, 87412, 89555, 90147]
      },
      {
        name: 'online',
        type: 'line',
        data: [124, 256, 358, 244, 555, 1201, 1900, 2489, 4890, 8569]
      }
    ]
  };
  shiftingPointOptions = {
    legend: {
      data: ['trovato', 'online'],
      align: 'left'
    },
    tooltip: {},
    xAxis: {
      type: 'category',
      data: ['1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        name: 'trovato',
        type: 'line',
        data: [75898, 77415, 77444, 77544, 79015, 81578, 86321, 87412, 89555, 90147]
      },
      {
        name: 'online',
        type: 'line',
        data: [124, 256, 358, 244, 555, 1201, 1900, 2489, 4890, 8569]
      },
      {
        name: 'online',
        type: 'line',
        data: [undefined, undefined, 358, 244],
        symbolSize: 10,
        lineStyle: {
          width: 0
        },
        itemStyle: {
          borderWidth: 5,
          borderColor: 'red',
          color: 'red'
        }
      }
    ]
  };

  constructor(
    protected activatedRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private timeSeriesResultService: TimeSeriesResultService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.corpusId = params['corpusId'];
      this.searchTerms = params['searchTerms'];
      this.firstTerms = params['firstTerms'] ? params['firstTerms'].split(',') : params['firstTerms'];
      this.secondTerms = params['secondTerms'] ? params['secondTerms'].split(',') : params['secondTerms'];
      this.startDate = params['startDate'];
      this.endDate = params['endDate'];
    });
    this.getResult();
    this.mapTerms();
    this.options = this.noShiftingPointOptions;
  }

  switchShiftingPoints(): void {
    this.shiftingPointsEnabled = !this.shiftingPointsEnabled;
    this.options = this.shiftingPointsEnabled ? this.shiftingPointOptions : this.noShiftingPointOptions;
    this.changeDetector.detectChanges();
  }

  private mapTerms(): void {
    if (this.firstTerms && this.secondTerms) {
      const len = this.firstTerms.length;
      for (let i = 0; i < len; i++) {
        this.terms += this.firstTerms[i] + '-' + this.secondTerms[i];
        if (i !== len - 1) {
          this.terms += ', ';
        }
      }
    } else if (this.searchTerms) {
      this.terms = this.searchTerms;
    }
  }

  protected onSuccess(): void {}

  protected onError(): void {}

  private getResult(): void {
    const params: Params = {
      corpusId: this.corpusId
    };
    if (this.searchTerms !== undefined) {
      params.searchTerms = this.searchTerms;
    }
    if (this.firstTerms !== undefined) {
      params.firstTerms = this.firstTerms;
    }
    if (this.secondTerms !== undefined) {
      params.secondTerms = this.secondTerms;
    }
    if (this.startDate !== undefined) {
      params.startDate = this.startDate;
    }
    if (this.endDate !== undefined) {
      params.endDate = this.endDate;
    }
    this.timeSeriesResultService.query(params).subscribe(
      () => this.onSuccess(),
      () => this.onError()
    );
  }
}
